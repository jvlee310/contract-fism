import random

random.seed(0)
import time

import multiprocessing as mp
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import numpy as np
import os


def slide_window(itemList, input_len):
    """
    Input a sequence [1, 2, 3, 4], and input len is 4
    Return    [2, 3, 4, 0], [1, 3, 4, 0], [1, 2, 4, 0], [2, 3, 4, 0]
    with      [1],  [2],  [3], [4]
    and mask: [1, 1, 1, 0], [1, 1, 1, 0] [1, 1, 1, 0] [1, 1, 1, 0]
    """
    append_item_list = [0] * (input_len - len(itemList) + 1)
    for mask_idx in range(len(itemList)):
        input_seq = itemList[0: mask_idx] + itemList[mask_idx + 1:] + append_item_list
        target = [itemList[mask_idx]]
        mask = [1] * (len(itemList) - 1) + append_item_list
        yield input_seq, target, mask


class DataPreprocessor(object):

    def __init__(self, config):
        print('#' * 10 + ' DataInfo ' + '#' * 10)
        self.input_len = config['input_len']
        self.device = config['device']
        self.data_path = './datasets/' + config['dataset'] + '/seq/'
        self.train_neg_num = config['train_neg_num']
        self.thread_num = config['thread_num']
        random.seed(123)
        np.random.seed(123)
        self.user2Idx = {}
        self.item2Idx = {'padding': 0}
        self.itemIdx2Str = {}
        self.userIdx2Str = {}
        self.userIdx2Items_train = {}
        self.userIdx2Items_test = {}
        self.userItemSet = {}
        self.itemUserList = {}
        self.item_freq = {}
        self.valid_users = set()
        self.valid_items = set()
        self.cpu_num = min(mp.cpu_count(), 2)

        self.numUser = 0
        self.numItem = 0
        self.item_dist = [0 for _ in range(self.numItem)]
        self.user_dist = [0 for _ in range(self.numUser)]
        self.load_data()

        config['user_num'] = self.numUser
        config['item_num'] = self.numItem
        print(f'numUser:{self.numUser}')
        print(f'numItem:{self.numItem}')
        self.eval_neg_num = config['eval_neg_num']
        self.train_batch_size = config['train_batch_size']
        self.test_batch_size = config['test_batch_size']
        self.train_size = 0
        self.valid_size = 0
        self.test_size = 0

    def load_data(self):
        max_len = 0
        full_seq_count = self.load_train_file('train.txt')
        full_test_count = self.load_test_file('test.txt')
        self.numUser = len(self.user2Idx)
        self.numItem = len(self.item2Idx)
        # build item distribution
        self.item_dist = [0 for _ in range(self.numItem)]
        self.user_dist = [0 for _ in range(self.numUser)]
        seq_len_sum = 0
        for user, item_seq in self.userIdx2Items_train.items():
            seq_len_sum += len(item_seq)
            if len(item_seq) > max_len:
                max_len = len(item_seq)
            for item in item_seq:
                self.item_dist[item] += 1
            self.user_dist[user] = len(item_seq)
        assert seq_len_sum == full_seq_count
        self.generate_item_dist()
        for user, items in self.userIdx2Items_train.items():
            self.userItemSet[user] = set(items)
        self.input_len = max_len

    def generate_train_dataloader(self):
        print('generating train samples')
        start = time.time()
        train_hist_items = []
        train_masks = []
        train_targets = []
        for user, item_full_seq in self.userIdx2Items_train.items():
            for input_seq, target, mask in slide_window(item_full_seq, self.input_len):
                train_hist_items.append(input_seq)
                train_masks.append(mask)
                train_targets.append(target[0])
                self.valid_users.add(user)
                self.valid_items.add(target[0])
                for item in input_seq:
                    if item is not 0:
                        self.valid_items.add(item)
        self.train_size = len(train_targets)
        print(f"train_size: {self.train_size}, time: {(time.time() - start)}")
        print(f"valid user num: {len(self.valid_users)}")
        print(f"valid item num: {len(self.valid_items)}")

        dataset = UnidirectTrainDataset(train_hist_items, train_masks, train_targets, self.userItemSet,
                                        max_item_idx=self.numItem - 1, neg_num=self.train_neg_num,
                                        item_dist=self.item_dist)
        dataloader = DataLoader(dataset, shuffle=True, num_workers=self.cpu_num, batch_size=self.train_batch_size)

        return dataloader

    def generate_test_dataloader(self):
        input_len = self.input_len
        print('generating test samples')
        start = time.time()
        test_users = []
        test_hist_items = []
        test_masks = []
        test_targets = []
        abandon_count = 0
        for user, target_items in self.userIdx2Items_test.items():
            train_items = self.userIdx2Items_train[user]
            append_list = [0] * (self.input_len - len(train_items))
            input_seq = train_items + append_list
            mask_seq = [1] * len(train_items) + append_list

            for target_item in target_items:
                test_hist_items.append(input_seq)
                test_masks.append(mask_seq)

                sampled_negs = []
                while len(sampled_negs) < 101:
                    sampled_neg_cands = np.random.choice(self.numItem, self.eval_neg_num, False, self.item_dist)
                    valid_neg_ids = [x for x in sampled_neg_cands if x not in self.userItemSet[user]]
                    sampled_negs.extend(valid_neg_ids[:])
                sampled_negs = sampled_negs[:100]
                test_targets.append([target_item] + list(sampled_negs))

        dataset = UnidirectTestDataset(test_hist_items, test_masks, test_targets)
        dataloader = DataLoader(dataset, shuffle=False,
                                num_workers=self.cpu_num, batch_size=self.test_batch_size)
        self.test_size = len(test_users)
        print(f"test_size: {self.test_size}, time: {(time.time() - start)}")
        return dataloader

    def generate_item_dist(self):
        # print('generating item distribution')
        item_dist = np.array(self.item_dist)
        sum_click = item_dist.sum()
        self.item_dist = item_dist / sum_click
        # print(f'item dist 0: {self.item_dist[0]}')
        self.item_dist[0] = 0

    def load_train_file(self, file_name):
        file_path = self.data_path + '/' + file_name
        line_count = 0
        if os.path.exists(file_path):
            print(f'reading {file_name}')
            with open(file_path) as fin:
                for line in fin:
                    splited_line = line.strip().split(' ')
                    user, item = splited_line[0], splited_line[1]
                    if user not in self.user2Idx:
                        userIdx = len(self.user2Idx)
                        self.user2Idx[user] = userIdx
                    if item not in self.item2Idx:
                        itemIdx = len(self.item2Idx)
                        self.item2Idx[item] = itemIdx
                    userIdx = self.user2Idx[user]
                    itemIdx = self.item2Idx[item]
                    if userIdx not in self.userIdx2Items_train:
                        self.userIdx2Items_train[userIdx] = []
                    self.userIdx2Items_train[userIdx].append(itemIdx)
                    line_count += 1
        return line_count

    def load_test_file(self, file_name):
        file_path = self.data_path + '/' + file_name
        line_count = 0
        if os.path.exists(file_path):
            print(f'reading {file_name}')
            with open(file_path) as fin:
                for line in fin:
                    splited_line = line.strip().split(' ')
                    user, item = splited_line[0], splited_line[1]
                    if user not in self.user2Idx:
                        continue
                    if item not in self.item2Idx:
                        continue
                    userIdx = self.user2Idx[user]
                    itemIdx = self.item2Idx[item]
                    if userIdx not in self.userIdx2Items_test:
                        self.userIdx2Items_test[userIdx] = []
                    self.userIdx2Items_test[userIdx].append(itemIdx)
                    line_count += 1
        return line_count


class UnidirectTrainDataset(torch.utils.data.Dataset):

    def __init__(self, train_hist_items,
                 train_masks, train_targets, userItemSet, max_item_idx, neg_num, item_dist, aug_num=3):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        train_masks = [bs, seq_len]
        train_targets = [bs]
        clean_mask = [bs], denoting whether this instance is clean or not
        """
        assert len(train_hist_items) == len(train_masks) == len(train_targets)
        self.train_hist_items = train_hist_items
        self.train_masks = train_masks
        self.train_targets = train_targets
        # self.clean_mask = clean_mask
        self.train_size = len(train_targets)
        self.userItemSet = userItemSet
        self.max_item_idx = max_item_idx
        self.neg_num = neg_num
        self.aug_num = aug_num
        self.input_len = len(self.train_hist_items[0])
        self.item_dist = item_dist
        self.numItem = len(item_dist)

    def __getitem__(self, index):
        interacted_items = set(self.train_hist_items[index] + [self.train_targets[index]])
        final_negs = []
        while len(final_negs) < self.neg_num:
            sampled_negs = np.random.choice(self.numItem, self.neg_num, False, self.item_dist)
            valid_negs = [x for x in sampled_negs if x not in interacted_items]
            final_negs.extend(valid_negs[:])
        final_negs = final_negs[:self.neg_num]
        return torch.tensor(self.train_hist_items[index]), \
               torch.tensor(self.train_masks[index]), \
               self.train_targets[index], \
               torch.tensor(final_negs), \
               index

    def __len__(self):
        return self.train_size


class UnidirectTestDataset(torch.utils.data.Dataset):

    def __init__(self, test_hist_items, test_masks, test_targets):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, pred_num]
        """
        self.test_hist_items = test_hist_items
        self.test_masks = test_masks
        self.test_targets = test_targets
        self.test_size = len(test_targets)

    def __getitem__(self, index):
        return torch.tensor(self.test_hist_items[index]), \
               torch.tensor(self.test_masks[index]), \
               torch.tensor(self.test_targets[index]),

    def __len__(self):
        return self.test_size