from data_utils.DataPreprocessor import *
import scipy.sparse as sp
from time import time

class GraphDataCollector(DataPreprocessor):

    def __init__(self, config):
        super().__init__(config)
        self.Graph = None