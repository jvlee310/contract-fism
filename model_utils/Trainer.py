from data_utils.RankingEvaluator import RankingEvaluator
import torch
from torch import optim
import time
import os
import numpy as np
from torch.optim.lr_scheduler import ReduceLROnPlateau
from model.GraphSeq import ProbGraphSeqRec
from model.FISM import FISM


class Trainer:
    def __init__(self, config, data_model, save_dir):

        train_loader = data_model.generate_train_dataloader()
        test_loader = data_model.generate_test_dataloader()

        self.config = config
        self.save_dir = save_dir
        self.train_type = config['train_type']
        self.rec_model = 'FISM'

        self.train_loader = train_loader
        self._evaluator_1 = RankingEvaluator(test_loader)
        self.item_dist = np.array(data_model.item_dist)
        self.user_dist = np.array(data_model.user_dist)
        self.train_size = len(train_loader.dataset)
        self.model_save_dir = './datasets/' + self.config['dataset'] + '/model/'
        self.model_save_path = self.model_save_dir + self.rec_model + '-'
        self.save_epochs = self.config['save_epochs']
        self.data_model = data_model

        rec_model_1 = ProbGraphSeqRec(config, FISM(config))

        if self.train_type == 'train':
            self._model_1 = rec_model_1
            self._device = config['device']
            self._model_1.double().to(self._device)
            self._optimizer_1 = _get_optimizer(
                self._model_1, learning_rate=config['learning_rate'], weight_decay=config['weight_decay'])
            self.scheduler_1 = ReduceLROnPlateau(self._optimizer_1, 'max', patience=10,
                                                 factor=config['decay_factor'])
            self.forget_rates = self.build_forget_rates()
        elif self.train_type == 'eval':
            self._device = config['device']
            self._model_1 = rec_model_1

    def build_forget_rates(self):
        forget_rates = np.ones(self.config['epoch_num']) * 0.06
        forget_rates[:20] = np.linspace(0, 0.06, 20)
        return forget_rates

    def save_model(self, epoch_num):
        if not os.path.exists(self.model_save_dir):
            os.makedirs(self.model_save_dir)
        save_path = self.model_save_path + str(epoch_num) + '-model.pkl'
        torch.save(self._model_1.context_encoder, save_path)
        print(f'model saved at {save_path}')

    def load_model(self, epoch_num):
        load_path = self.model_save_path + str(epoch_num) + '-model.pkl'
        print(f'loading model from {load_path}')
        return torch.load(load_path)

    def co_train_one_batch(self, batch, epoch_num, batch_num):
        self._model_1.train()
        self._optimizer_1.zero_grad()
        # [bs], [bs], [bs]
        overall_loss_1, sample_idices = self._model_1(batch)

        overall_loss_1.backward()
        self._optimizer_1.step()

        return overall_loss_1

    def run_co(self):
        if self.train_type == 'train':
            print('=' * 60, '\n', 'Start Training', '\n', '=' * 60, sep='')
            keep_train = True
            for epoch in range(self.config['epoch_num']):
                start = time.time()
                loss_1_iter = 0
                for i, batch in enumerate(self.train_loader):
                    loss_1 = self.co_train_one_batch(batch, epoch, i)
                    loss_1_iter += loss_1.item()
                print(f'################## epoch {epoch} ###########################')
                print(f"loss: {round(loss_1_iter / len(self.train_loader), 4)}, len_train_loader:{len(self.train_loader)}")
                keep_train = self.evaluate(epoch)
                print('#########################################################')
                if epoch in self.save_epochs:
                    self.save_model(epoch)
                if not keep_train:
                    break
        elif self.train_type == 'eval':
            for epoch in self.save_epochs:
                self._model_1.set_encoder(self.load_model(epoch))
                self._model_1.double().to(self._device)
                self._evaluator_1.evaluate(model=self._model_1, train_iter=0)

    def evaluate(self, iter):
        self._model_1.eval()
        keep_train_1, ndcg10_1 = self._evaluator_1.evaluate(model=self._model_1, train_iter=iter)
        self.scheduler_1.step(ndcg10_1)

        return keep_train_1

    def predict_by_userId(self, userId):
        input_items = self.data_model.userIdx2Items_train[userId]
        append_list = [0] * (self.data_model.input_len - len(input_items))
        input_masks = [1] * len(input_items) + append_list
        targets = list(set(range(1, self.data_model.numItem)) - set(input_items))

        input_items_tensor = torch.tensor(input_items + append_list)
        input_masks_tensor = torch.tensor(input_masks)
        targets_tensor = torch.tensor(targets)

        predicted_scores = self._model_1.get_test_scores(input_items_tensor, input_masks_tensor, targets_tensor)





    @property
    def model(self):
        return self._model_1

    @property
    def optimizer(self):
        return self._optimizer_1


def _get_optimizer(model, learning_rate, weight_decay=0.01):
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if
                    not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
        {'params': [p for n, p in param_optimizer if
                    any(nd in n for nd in no_decay)], 'weight_decay': 0.0}]

    return optim.Adam(optimizer_grouped_parameters, lr=learning_rate)


def set2str(input_set):
    set_str = ''
    set_len = len(input_set)
    for i, item in enumerate(input_set):
        if i < set_len - 1:
            set_str += str(item) + ','
        else:
            set_str += str(item)
    return set_str

