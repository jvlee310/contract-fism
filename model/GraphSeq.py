import torch
from torch import nn
import math


class ContextEncoder(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.user_embeddings = nn.Embedding(config['user_num'], config['hidden_size'])
        self.item_embeddings = nn.Embedding(config['item_num'], config['hidden_size'], padding_idx=0)

    def forward(self, hist_item_ids, masks):
        raise NotImplementedError

class GraphSeqRec(nn.Module):
    def __init__(self, config, context_encoder: ContextEncoder):
        super().__init__()
        self.config = config
        if self.config['train_type'] == 'train':
            self.context_encoder = context_encoder
            self.apply(self._init_parameters)

    def set_encoder(self, encoder):
        self.context_encoder = encoder

    def _init_parameters(self, module):
        """ Initialize the parameterss.
        """
        if isinstance(module, nn.Embedding):
            hidden_size = module.weight.size()[1]
            bound = 6 / math.sqrt(hidden_size)
            nn.init.uniform_(module.weight, a=-bound, b=bound)
        if isinstance(module, nn.Linear):
            torch.nn.init.xavier_normal_(module.weight, gain=1.0)
        if isinstance(module, nn.Linear) and module.bias is not None:
            module.bias.data.zero_()

    def forward(self, train_batch):
        user_id, hist_item_ids, masks, pos_target, neg_targets, sample_idices = train_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_targets = neg_targets.to(torch.device('cuda'))
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_targets = [bs, neg_num]
        """
        user_prop_embeds, item_prop_embeds = self.propagate_embeds(is_training=True)
        neg_num = neg_targets.size()[1]
        # [bs, hidden_size]
        context_embed = self.context_encoder(user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds)
        # [bs, 1]
        pos_score = torch.mul(context_embed, item_prop_embeds(pos_target)).sum(dim=1, keepdim=True)
        # [bs, neg_num, hidden_size]
        neg_embeds = item_prop_embeds(neg_targets)
        # [bs, neg_num]
        neg_scores = torch.mul(context_embed.unsqueeze(1), neg_embeds).sum(dim=2, keepdim=False)
        # [bs], BPR loss
        overall_loss = -(torch.log(torch.sigmoid(pos_score - neg_scores)) / neg_num).sum(dim=1, keepdim=False)

        return overall_loss, sample_idices

    def eval_ranking(self, test_batch):
        hist_item_ids, masks, target_ids = test_batch
        masks = masks.float()
        if torch.cuda.is_available():
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.double().to(torch.device('cuda'))
            target_ids = target_ids.to(torch.device('cuda'))
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, eval_neg_num + 2]
        """
        ranks = self.get_test_scores(hist_item_ids=hist_item_ids, masks=masks, target_ids=target_ids).argsort(
            dim=1, descending=True).argsort(dim=1, descending=False)[:, 0:1].float()

        # evaluate ranking
        metrics = {
            'ndcg_1': 0,
            'ndcg_5': 0,
            'ndcg_10': 0,
            'ndcg_20': 0,
            'hit_1': 0,
            'hit_5': 0,
            'hit_10': 0,
            'hit_20': 0,
            'ap': 0,
        }
        for rank in ranks:
            if rank < 1:
                metrics['ndcg_1'] += 1
                metrics['hit_1'] += 1
            if rank < 5:
                metrics['ndcg_5'] += 1 / torch.log2(rank + 2)
                metrics['hit_5'] += 1
            if rank < 10:
                metrics['ndcg_10'] += 1 / torch.log2(rank + 2)
                metrics['hit_10'] += 1
            if rank < 20:
                metrics['ndcg_20'] += 1 / torch.log2(rank + 2)
                metrics['hit_20'] += 1
            metrics['ap'] += 1.0 / (rank + 1)
        return metrics

    def get_test_scores(self, hist_item_ids, masks, target_ids):
        """
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, 1 + eval_neg_num]
        """
        # [bs, 1, hidden_size]
        context_embed = self.context_encoder(hist_item_ids, masks).unsqueeze(1)
        # [bs, pred_num, hidden_size]
        target_embeds = self.context_encoder.item_embeddings.weight[target_ids]
        # [bs, pred_num]
        scores = torch.mul(context_embed, target_embeds).sum(dim=2, keepdim=False)

        return scores


class ProbGraphSeqRec(GraphSeqRec):

    def __init__(self, config, context_encoder: ContextEncoder):
        super().__init__(config, context_encoder)

    def forward(self, train_batch):
        hist_item_ids, masks, pos_target, neg_targets, sample_idices = train_batch
        if torch.cuda.is_available():
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda')).long()
            neg_targets = neg_targets.to(torch.device('cuda')).long()
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_targets = [bs, neg_num]
        """
        pos_target = pos_target.unsqueeze(1)
        # [bs, hidden_size]
        context_mean = self.context_encoder(hist_item_ids, masks)
        # [bs, 1. hidden_size]
        unseq_context_mean = context_mean.unsqueeze(1)

        # [bs, 1], [bs, sample_num, 1]
        pos_score = self.prediction_score(unseq_context_mean, pos_target, self.context_encoder.item_embeddings.weight)
        # [bs, neg_num], [bs, sample_num, neg_num]
        neg_scores = self.prediction_score(unseq_context_mean, neg_targets, self.context_encoder.item_embeddings.weight)

        # [bs], BPR loss
        # [bs, 1] - [bs, neg_num] = [bs, neg_num] -sum-> [bs]
        rec_loss = -torch.log(torch.sigmoid(pos_score - neg_scores) + 1e-8).mean(dim=1, keepdim=False).sum()

        return rec_loss, sample_idices


    def prediction_score(self, context_mean, target_item_ids, prop_item_embeds):
        """
        context_mean = [bs, 1, hidden_size]
        context_var = [bs, 1, hidden_size]
        target_item_ids = [bs, pred_num]
        prop_item_embeds = [itemNum, hidden_size]
        """
        # [bs, pred_num, hidden_size]
        target_item_embed = prop_item_embeds[target_item_ids.long()]
        # [bs, pred_num]
        W_scores = torch.mul(context_mean, target_item_embed).sum(dim=2, keepdim=False)

        return W_scores