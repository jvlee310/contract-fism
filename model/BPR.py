import torch
from torch import nn
from model_utils.BERT_SeqRec import BertModel
from model_utils.BERT_SeqRec import BertConfig
# from model_utils.Utils import normalize

from model.GraphSeq import RobGraphContextEncoder


class BERD(RobGraphContextEncoder):

    def __init__(self, config):
        super().__init__(config)
        self.fc_var = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()
        self.fc_embed = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()
        self.seq_module = None
        self.network_param_init(config)

    def network_param_init(self, config):
        bert_config = BertConfig(config['item_num'], config)
        self.seq_module = BertModel(bert_config, use_outer_embed=True)

    def seq_modelling(self, hist_item_ids, masks, item_prop_embeds,
                      user_embed=None):
        # [bs, seq_len, hidden_size]
        bert_context = self.seq_module(hist_item_ids, attention_mask=masks, outer_embed=item_prop_embeds)
        # [bs, hidden_size]
        return bert_context[:, -1, :].squeeze(1)

    def forward(self, user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        user_mean = user_prop_embeds[user_id]
        # [bs, hidden_size]
        item_context_embed = self.seq_modelling(hist_item_ids, masks, item_prop_embeds, user_mean)
        context_embed = self.fc_embed(torch.cat((item_context_embed, user_mean), dim=1))
        context_var = self.fc_var(torch.cat((item_context_embed, user_mean), dim=1))

        # [bs, 1]
        user_var = self.user_var(user_id).unsqueeze(1)
        # [bs, seq_len, 1]
        item_vars = self.item_var(hist_item_ids)
        # [bs, seq_len + 1, 1]
        all_var = torch.cat([user_var, item_vars], dim=1)
        # [bs, 1]
        mean_var = torch.sigmoid(torch.mean(all_var, dim=1, keepdim=False))

        # # [bs, rel_num, dim], [bs, rel_num, 1]
        # merged_embeds, weights = self.content_modeling(hist_item_ids)

        # max_weight = 1 - weights.max(dim=1, keepdim=False)[0]
        return context_embed, torch.relu(context_var * mean_var) # , merged_embeds, weights


    # def content_modeling(self, hist_item_ids):
    #     weights = []
    #     merged_embeds = []
    #     # [bs, input_len, dim]
    #     hist_item_embeds = self.get_item_entity_embed()[hist_item_ids]
    #     for rel_idx in range(self.config['relation_num']):
    #         rel_idx_tensor = torch.tensor([rel_idx])
    #         if torch.cuda.is_available():
    #             rel_idx_tensor = rel_idx_tensor.to(torch.device('cuda'))
    #         # [1, 1, dim]
    #         rel_embed = self.relation_embeddings(rel_idx_tensor).unsqueeze(0)
    #         transe_item_embeds = normalize(hist_item_embeds) + normalize(rel_embed)
    #         # [bs, 1, dim], [bs, 1, 1]
    #         merged_embed, weight = self.merge_embed(transe_item_embeds)
    #         merged_embeds.append(merged_embed)
    #         weights.append(weight)
    #     # [bs, rel_num, dim]
    #     merged_embeds = torch.cat(merged_embeds, dim=1)
    #     # [bs, rel_num, 1]
    #     weights = torch.cat(weights, dim=1)
    #
    #     return merged_embeds, weights

    # def merge_embed(self, normalized_embeds, iter_num=1):
    #     """
    #     :param normalized_embeds: # [bs, input_len, dim]
    #     :return: merged_embed: [bs, 1, dim], concentrate_factor: [bs, 1, 1]
    #     """
    #     # [bs, input_len, dim]
    #     # squased_embeds = self.squash(embeddings)
    #     weighted_sum = None
    #     # [bs, input_len, 1]
    #     distances = torch.ones(normalized_embeds.shape[0], normalized_embeds.shape[1], 1)
    #     if torch.cuda.is_available():
    #         distances = distances.to(torch.device('cuda')).double()
    #     for i in range(iter_num):
    #         # [bs, input_len, 1]
    #         normalized_distances = distances / distances.sum(dim=1, keepdim=True)
    #         # [bs, 1, dim]
    #         weighted_sum = (normalized_distances * normalized_embeds).sum(dim=1, keepdim=True)
    #         # [bs, input_len]
    #         distances = (normalized_embeds - weighted_sum).pow(2).sum(dim=2, keepdim=False)
    #     # [bs, 1, dim], [bs, 1, 1]
    #     return weighted_sum, weighted_sum.pow(2).sum(dim=2, keepdim=True) * 0.5






