# Contract Clause Recommendation with FISM

## Requirements
- `Anaconda==4.8.5`
- `python==3.6`
- `pytorch==1.1.0`
- `CUDA==10.1`
- `cuDNN==7.5.1`


## Usage
1. Install required packages.
2. run </code>python main.py</code> to train and evaluate FISM with demo dataset. 
